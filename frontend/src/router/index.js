import Vue from 'vue'
import Router from 'vue-router'
import 'uikit/dist/css/uikit.css'
import UIkit from 'uikit'
import Icons from 'uikit/dist/js/uikit-icons'


import Index from '@/components/pages/index'
import Checkout from '@/components/pages/checkout'

Vue.use(Router)
// loads the Icon plugin
UIkit.use(Icons);

export default new Router({
  routes: [
    { path: '/', name: 'Index', component: Index },
    { path: '/checkout', name: 'Checkout', component: Checkout }
  ]
})
