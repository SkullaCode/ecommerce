<?php

require_once('../vendor/autoload.php');
session_start();

$config = [
    'settings'  =>  [
        'addContentLengthHeader'    =>  false
    ]
];

$app = new \Slim\App($config);

$container = $app->getContainer();
$container['view'] = new \Slim\Views\PhpRenderer('../views/');

$app->get('/',function($request,$response,$args){
    if(!isset($_SESSION['cart']))
    {
        $_SESSION['cart'] = [];
        $_SESSION['charge_id'] = [];
        $_SESSION['data'] = [];
    }

    return $this->view->render($response,'index.html');
});

$app->get('/process[/{id}]',function(\Slim\Http\Request $rq,\Slim\Http\Response $rs){

    try
    {
        return (is_null($rq->getParam('id')))
            ? $rs->getBody()->write(json_encode([
                'transactions'  =>  $_SESSION['charge_id'],
                'details'       =>  $_SESSION['data'],
                'transaction'   =>  null
            ]))
        : $rs->getBody()->write(json_encode([
            'transactions'  =>  $_SESSION['charge_id'],
            'details'       =>  $_SESSION['data'],
            'transaction'   =>  [
                'id'    =>  $rq->getParam('id'),
                $_SESSION['data'][$rq->getParam('id')]
            ]
        ]));
    }
    catch (ErrorException $e)
    {
        return $rs->getBody()->write(json_encode([
            'transactions'  =>  $_SESSION['charge_id'],
            'details'       =>  $_SESSION['data'],
            'transaction'   =>  null
        ]));
    }
});

/**
 * POST request processes transaction
 * username is required, send in request
 */
$app->post('/process',function(\Slim\Http\Request $rq,\Slim\Http\Response $rs){

    if(isset($_SESSION['cart']))
    {
        \Stripe\Stripe::setApiKey('sk_test_zesIDDg5pJI218oAPASGduVZ');

        $amount = 0;
        foreach($_SESSION['cart'] as $cart_item)
        {
            if(empty($cart_item['itemQuantity']))
                continue;
            $amount += ($cart_item['itemPrice'] * $cart_item['itemQuantity']);
        }

        try
        {
            /**
             * @var \Stripe\Charge $charge
             */
            $charge = \Stripe\Charge::create([
                'amount'        =>  $amount,
                'currency'      =>  'JMD',
                'description'   =>  'Charge for '.$rq->getParam('stripeEmail'),
                'source'        =>  $rq->getParam('stripeToken')
            ]);
            if($charge->status === "succeeded")
            {
                //empty cart on successful transaction
                $_SESSION['cart'] = [];
            }
            $_SESSION['charge_id'][] = $charge->id;
            $_SESSION['data'][$charge->id] = [
                'amount'                =>  number_format(($charge->amount/100),2),
                'balance_transaction'   =>  $charge->balance_transaction,
                'created'               =>  $charge->created,
                'currency'              =>  $charge->currency,
                'email'                 =>  $charge->source->name,
                'card_source'           =>  $charge->source->id,
                'cart'                  =>  $_SESSION['cart']
            ];
            return $rs->getBody()->write(json_encode([
                'transactions'  =>  $_SESSION['charge_id'],
                'details'       =>  $_SESSION['data'],
                'transaction'   =>  [
                    'id'    =>  $charge->id,
                    $_SESSION['data'][$charge->id]
                ]
            ]));
        }
        catch(Error $e)
        {
            return $rs->getBody()->write(json_encode([
                'transactions'  =>  $_SESSION['charge_id'],
                'details'       =>  $_SESSION['data'],
                'transaction'   =>  null
            ]));
        }
    }
    return $rs->getBody()->write(json_encode([
        'transactions'  =>  $_SESSION['charge_id'],
        'details'       =>  $_SESSION['data'],
        'transaction'   =>  null
    ]));
})->add(new \Ecommerce\Register());

$app->get('/cart',function(\Slim\Http\Request $rq, \Slim\Http\Response $rs){
    $cart_item = (isset($_SESSION['cart'])) ? $_SESSION['cart'] : [];
    return $rs->getBody()->write(json_encode($cart_item));
});

/**
 * POST request adds an item to the cart
 */
$app->post('/cart',function(\Slim\Http\Request $rq,\Slim\Http\Response $rs){
    if(!isset($_SESSION['cart']))
        $_SESSION['cart'] = [];

    $_SESSION['cart'][] = [
        'cartID'        =>  substr(md5(mktime()),15,6),  //use this to update quantity and delete item
        'itemID'        =>  $rq->getParam('itemID'),  //send this
        'itemPrice'     =>  ((int)$rq->getParam('itemPrice') * 100),//send this //stripe works with cents
        'itemName'      =>  $rq->getParam('itemName'), //send this
        'itemQuantity'  =>  (int)$rq->getParam('itemQuantity') // send this
    ];
    return $rs->getBody()->write(json_encode($_SESSION['cart']));
});

/**
 * PUT request updates the quantity of an item in the cart
 */
$app->put('/cart',function(\Slim\Http\Request $rq, \Slim\Http\Response $rs){
    $id = $rq->getParam('cartID');
    if(!is_null($id) && isset($_SESSION['cart']))
    {
        for($i=0; $i<count($_SESSION['cart']);$i++)
        {
            if($id === $_SESSION['cart'][$i]['cartID'])
            {
                $_SESSION['cart'][$i]['itemQuantity'] = $rq->getParam('itemQuantity');
                break;
            }
        }
        return $rs->getBody()->write(json_encode($_SESSION['cart']));
    }
    return $rs->getBody()->write(json_encode($_SESSION['cart']));
});

/**
 * DELETE request deletes an item from the cart
 */
$app->delete('/cart',function(\Slim\Http\Request $rq, \Slim\Http\Response $rs){
    $id = $rq->getParam('cartID');
    if(!is_null($id) && isset($_SESSION['cart']))
    {
        for($i=0; $i<count($_SESSION['cart']); $i++)
        {
            if($id === $_SESSION['cart'][$i]['itemID'])
            {
                unset($_SESSION['cart'][$i]);
                break;
            }
        }
        $_SESSION['cart'] = array_values($_SESSION['cart']);  //re-order the array
        return $rs->getBody()->write(json_encode($_SESSION['cart']));
    }
    return $rs->getBody()->write(json_encode($_SESSION['cart']));
});

/**
 * DELETE request to /empty-cart removes all items
 */
$app->delete('/empty-cart',function(\Slim\Http\Request $rq, \Slim\Http\Response $rs){
    if(isset($_SESSION['cart']))
        unset($_SESSION['cart']);
    $_SESSION['cart'] = [];
    return $rs->getBody()->write(json_encode($_SESSION['cart']));
});

$app->get('/logout',function(\Slim\Http\Request $rq,\Slim\Http\Response $rs){
    $_SESSION = [];
    session_destroy();
    return $rs->withRedirect('/');
});

try
{
    $app->run();
}
catch (\Slim\Exception\MethodNotAllowedException $e)
{

}
catch (\Slim\Exception\NotFoundException $e)
{

}
catch (Exception $e)
{

}