<?php


namespace Ecommerce;


class DataGenerator
{
    public static function YearList()
    {
        return [
            '2018',
            '2019',
            '2020',
            '2021',
            '2022'
        ];
    }

    public static function MonthList()
    {
        return [
            'January','February','March',
            'April','May','June',
            'July','August','September',
            'October','November','December'
        ];
    }
}