<?php

namespace Ecommerce;


use Slim\Http\Request;
use Slim\Http\Response;

class Register
{
    public function __invoke(Request $rq,Response $rs,$next)
    {
        if(!isset($_SESSION['registry'])){
            $username = $rq->getParam('username');
            if(!is_null($username))
            {
                $_SESSION['registry']['username'] = $username;
                return $next($rq,$rs);
            }
            return $rs->withStatus(500,'User not registered');
        }
        return $next($rq,$rs);
    }
}